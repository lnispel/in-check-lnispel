# /app/helpers/application_helper.rb
module ApplicationHelper
  # Returns Full Titles on per-page basis
  def full_title(page_title = '')
    base_title = 'Appointment Manager'
    if page_title.empty?
      base_title
    else
      page_title + ' | ' + base_title
    end
  end
end
