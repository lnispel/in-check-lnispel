# /app/controllers/static_pages_controller.rb
class StaticPagesController < ApplicationController
  def home
    @appointment = current_user.appointments.build if logged_in?
    @feed_items = current_user.feed.paginate(page: params[:page]) if logged_in?
  end

  def help; end

  def about; end

  def contact; end
end
