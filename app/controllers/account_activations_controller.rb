# app/controllers/account_activations_controller.rb
class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      activate
    else
      flash[:danger] = 'Invalid activation link'
      redirect_to root_url
    end
  end

  def activate
    user = User.find_by(email: params[:email])
    user.activate
    user.update_attributes(activated: true, activated_at: Time.zone.now)
    log_in user
    flash[:success] = 'Account activated!'
    redirect_to user
  end
end
