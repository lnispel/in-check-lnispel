# /app/controllers/appointments_controller.rb
class AppointmentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :update]
  before_action :correct_user,   only: :destroy
  before_action :admin_user,     only: :destroy

  def create
    @appointment = current_user.appointments.build(appointment_params)
    if @appointment.save
      flash[:success] = 'Appointment created!'
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def edit
    @appointment = current_user.appointments.find_by(id: params[:id])
  end

  def update
    @appointment = current_user.appointments.find_by(id: params[:id])
    if @appointment.update_attributes(appointment_params)
      flash[:success] = 'Appointment updated!'
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def show
    @user = User.find_by(id: params[:format])
    @appointment = @user.appointments.find_by(id: params[:id])
    commenting
  end

  def commenting
    @comment = current_user.comments.build(params[:comment]) if logged_in?
    @comments = @appointment.feed.paginate(page: params[:page])
  end

  def destroy
    @appointment.destroy
    flash[:success] = 'Appointment deleted'
    redirect_to request.referer || root_url
  end

  private

  def appointment_params
    params.require(:appointment).permit(:name,
                                        :start_time,
                                        :end_time,
                                        :location,
                                        :phone_number)
  end

  def correct_user
    @appointment = current_user.appointments.find_by(id: params[:id])
    redirect_to root_url if @appointment.nil?
  end

  # Confimrs admin user
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
