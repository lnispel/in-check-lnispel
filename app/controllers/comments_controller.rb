# /app/controllers/comments_controller.rb
class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :update]
  before_action :admin_user,     only: [:destroy, :update]
  before_action :create,         only: :create

  def create
    @appointment = Appointment.find_by(id: params[:appointment_id])
    @comment = @appointment.comments.build(comment_params)
    @comment.appointment = @appointment
    @comment.user = current_user
    comment_flash
  end

  def comment_flash
    if @comment.save
      flash[:success] = 'Comment created!'
    else
      flash[:danger] = 'Enter content'
    end
    redirect_to request.referer || root_url
  end

  def find_user
    @user = User.find_by(id: params[:id])
  end

  def destroy
    @comment = Comment.find_by(id: params[:id])

    if @comment.destroy
      flash[:success] = 'Comment deleted'
    else
      flash[:danger] = 'Comment could not bedeleted'
    end
    redirect_to request.referer || root_url
  end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end

  def correct_user
    @comment = current_user.comments.find_by(id: params[:id])
    redirect_to root_url if @comment.nil?
  end

  # Confimrs admin user
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
