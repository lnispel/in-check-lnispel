# /app/models/appointment.rb
class Appointment < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy
  before_validation :strip_phone
  default_scope(-> { order(created_at: :desc) })
  validates :user_id, presence: true
  validates :name, presence: true, length: { maximum: 140 }
  validates :location, length: { maximum: 140 }
  validates :start_time, presence: true
  validates_datetime :end_time, :end_time, after: :start_time
  validates :phone_number, length: { minimum: 10 },
                           format: { with: /[0-9._]/,
                                     message: 'Can only contain numbers.' }
  after_create :reminder
  after_create :alert

  # Notify our appointment attendee X minutes before the appointment time

  def twilio_keys
    @twilio_number = Rails.application.secrets.TWILIO_NUMBER
    @client =
      Twilio::REST::Client.new Rails.application.secrets.TWILIO_ACCOUNT_SID,
                               Rails.application.secrets.TWILIO_AUTH_TOKEN
  end

  def reminder
    time_str = convert_time(start_time)
    reminder = "Hi #{user.name}.
    Just a reminder that you have an appointment coming up at
    #{time_str}"
    twilio_keys
    message_template(reminder)
  end

  def when_to_run_reminder
    start_time - 60.minutes
  end

  handle_asynchronously :reminder, run_at: proc { |i| i.when_to_run_reminder }

  def alert
    time_str = convert_time(start_time)
    alert = "Hi #{user.name}.
    Just a reminder that you have an appointment that just started at
    #{time_str}."
    twilio_keys
    message_template(alert)
  end

  def message_template(message)
    message = @client.account.messages.create(
      from:  @twilio_number,
      to: ('1' + user[:phone]),
      body: message
    )
    log message.to
  end

  def when_to_run_alert
    start_time
  end

  handle_asynchronously :alert, run_at: proc { |i| i.when_to_run_alert }

  def find_time
    distance_of_time_in_words(Time.zone.now, due_at)
  end

  def convert_time(datetime)
    datetime.strftime('%I:%M %P on %b. %d, %Y')
  end

  def strip_phone
    phone_number.gsub!(/[^0-9]/, '')
  end

  # Returns a appointment's status feed.
  def feed
    comment_ids = "SELECT id FROM comments
                     WHERE appointment_id = :appointment_id"
    Comment.where("id IN (#{comment_ids})
                     OR id = :appointment_id", appointment_id: id)
  end
end
