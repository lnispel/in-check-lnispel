# /db/migrate/20170405065238_create_appointments.rb
class CreateAppointments < ActiveRecord::Migration[5.0]
  def change
    create_table :appointments do |t|
      t.string :name
      t.string :location
      t.string :phone_number
      t.datetime :start_time
      t.datetime :end_time
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :appointments, [:user_id, :created_at]
  end
end
