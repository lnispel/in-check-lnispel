# /db/migrate/20170406013336_create_delayed_jobs.rb
class CreateDelayedJobs < ActiveRecord::Migration
  # Allows some jobs to jump to the front of the queue
  # Provides for retries, but still fail eventually.
  # YAML-encoded string of the object that will do work
  # YAML-encoded string of the object that will do work
  # When to run. Could be Time.zone.now for immediately,
  # or sometime in the future.
  # Set when a client is working on this object
  # Set when all retries have failed (actually, by default,
  # the record is deleted instead)
  # Who is working on this object (if locked)
  # The name of the queue this job is in
  def self.up
    create_table :delayed_jobs, force: true do |table|
      table.integer :priority, default: 0, null: false
      table.integer :attempts, default: 0, null: false
      table.text :handler,                 null: false
      table.text :last_error
      table.datetime :run_at
      table.datetime :locked_at
      table.datetime :failed_at
      table.string :locked_by
      table.string :queue
      table.timestamps null: true
    end

    add_index :delayed_jobs, [:priority, :run_at], name: 'delayed_jobs_priority'
  end

  def self.down
    drop_table :delayed_jobs
  end
end
