# /db/migrate/20170403055737_create_users.rb
class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
