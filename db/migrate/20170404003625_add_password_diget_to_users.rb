# /db/migrate/20170404003625_add_password_diget_to_users.rb
class AddPasswordDigetToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :password_digest, :string
  end
end
