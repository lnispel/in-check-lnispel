User.create!(name:  'Luke Nispel',
             email: 'lnispel@me.com',
             phone: '4023108842',
             password:              'foobar',
             password_confirmation: 'foobar',
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  'Example User',
             email: 'example2@railstutorial.org',
             phone: Faker::PhoneNumber.cell_phone,
             password:              'foobar',
             password_confirmation: 'foobar',
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  phone = Faker::PhoneNumber.cell_phone
  email = "example-#{n + 1}@railstutorial.org"
  password = 'password'
  User.create!(name:  name,
               phone: phone,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
  name = Faker::Name.name
  location = Faker::Address.street_name
  phone_number = Faker::PhoneNumber.cell_phone
  start_time = Faker::Time.forward(70, :morning)
  end_time = start_time + 1
  users.each do |user|
    user.appointments.create!(name: name,
                              location: location,
                              phone_number: phone_number,
                              start_time: start_time,
                              end_time: end_time)
  end
end

# Following relationships
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
