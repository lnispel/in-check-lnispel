Rails.application.routes.draw do
  get 'password_resets/edit'
  get 'password_resets/new'
  get 'sessions/new'
  get 'users/new'
  root 'static_pages#home'
  get '/help',      to: 'static_pages#help'
  get '/about',     to: 'static_pages#about'
  get '/contact',   to: 'static_pages#contact'
  get '/signup',    to: 'users#new'
  post '/signup',   to: 'users#create'
  get '/login',     to: 'sessions#new'
  post '/login',    to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get 'appointments/edit', to: 'appointments#update'
  resources :comments
  resources :users do
    member do
      get :following, :followers
      get :appointments
      get :comments
    end
    resources :comments
    resources :appointments do
      member do
        get :comments
      end
      resources :comments
    end
  end
  resources :appointments do
    member do
      get :comments
    end
    resources :comments
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :relationships,       only: [:create, :destroy]
end
