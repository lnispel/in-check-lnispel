require 'test_helper'

class AppointmentsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @appointment = appointments(:one)
  end

  test 'should redirect create when not logged in' do
    assert_no_difference 'Appointment.count' do
      post appointments_path(@appointment)
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy when not logged in' do
    assert_no_difference 'Appointment.count' do
      delete appointment_path(@appointment)
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy for wrong appointment' do
    log_in_as(users(:michael))
    appointment = appointments(:two)
    assert_no_difference 'Appointment.count' do
      delete appointment_path(appointment)
    end
    assert_redirected_to root_url
  end
end
