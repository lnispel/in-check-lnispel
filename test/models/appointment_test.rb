require 'test_helper'

class AppointmentTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    # This code is not idiomatically correct.
    @appointment = Appointment.new(name: 'Lunch with mom',
                                   start_time: Time.zone.now,
                                   end_time: Time.zone.now,
                                   phone_number: '4023108842',
                                   location: 'Neverland',
                                   user_id: @user.id)
  end

  test 'should be valid' do
    assert @appointment.valid?
  end

  test 'user id should be present' do
    @appointment.user_id = nil
    assert_not @appointment.valid?
  end
end
